from django.contrib import admin

from .models import Participants, Study

admin.site.register(Participants)
admin.site.register(Study)