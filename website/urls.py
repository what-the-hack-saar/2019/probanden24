from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add_edit_study/', views.add_edit_study, name='add_edit_study'),
    path('save_study/', views.save_study, name='save_study'),
    path('study_details/', views.study_details, name='study_details'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/register/', views.register, name='register'),
    path('accounts/profile/', views.profile, name='profile'),
    path('participate/', views.participate, name='participate'),
    path('accounts/profile/edit_profile', views.edit_profile, name='edit_profile')

]

