from django.contrib.auth import login, authenticate, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.http import HttpResponseNotFound, HttpResponseNotAllowed, HttpResponse
from django.shortcuts import render, redirect

from website.models import Participants, Study
from .forms.forms import RegisterForm, CreateStudyForm
from .forms.forms import UpdateUserForm


def index(request):
    studies = Study.objects.all()
    return render(request, 'index.html', context={"studies": studies})


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.participants.requirements = form.cleaned_data.get('requirements')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('profile')
    else:
        form = RegisterForm()
    return render(request, 'registration/register.html', {'form': form})


def login_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        redirect('profile')
    else:
        # todo show invalid login message
        pass


def logout_view(request):
    logout(request)
    redirect('index')


@login_required
def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
    else:
        return HttpResponseNotAllowed(['POST'])


@login_required
def save_study(request):
    if request.method == 'POST':
        title = request.POST['title']
        reward = request.POST['reward']
        description = request.POST['description']
        study = Study(title=title, reward=reward, description=description,
                      creator=Participants.objects.all().get(user=request.user))
        study.save()
        return render(request, 'study_details.html', context={'study': study})
    else:
        return HttpResponse("Nope!")


@login_required
def add_edit_study(request):
    if request.method == 'POST':
        # fill form, since we are just editing a study
        form = CreateStudyForm(request.POST)
        heading = "Edit study"
    else:
        # empty form, since we are adding a study
        form = CreateStudyForm()
        heading = "Add study"

    return render(request, 'add_edit_study.html', {'form': form, 'heading': heading})


def study_details(request):
    if request.method == 'POST':
        study_id = request.POST['studyId']
        study = Study.objects.only().get(id=study_id)

        return render(request, 'study_details.html', context={'study': study})
    else:
        return HttpResponse("Nope!")


@login_required
def profile(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            participant = Participants.objects.get(id=request.user.id)

            created_studies = participant.created_studies.all()
            registered_studies = participant.registered_studies.all()

            return render(request, 'profile.html',
                          context={'created_studies': created_studies,
                                   'registered_studies': registered_studies})
        else:
            return HttpResponseNotFound('<h1> User not authenticated </h1>')
    else:
        return render(request, 'profile.html', context={
            'studies': [{'title': 'Große Ohren', 'reward': '10000$'}]})


# todo use form like below in edit_profile
def participate(request):
    if request.user.is_authenticated:
        # if this is a POST request we need to process the form data
        if request.method == 'POST':
            # create a form instance and populate it with data from the request:
            if 'studyId' in request.POST:
                study_id = request.POST['studyId']
            else:
                raise Exception('studyId is not set')

        else:
            raise Exception('participate can only be called as POST')

        study = Study.objects.get(id=study_id)
        study.participants.add(Participants.objects.get(id=request.user.id))
        study.save()

        return redirect('profile')
    else:
        # todo return login page
        return HttpResponseNotFound('<h1>Page not found</h1>')


@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = UpdateUserForm(data=request.POST, instance=request.user)
        if form.is_valid():
            if form.has_changed():
                user = form.save()
                user.save()
                return redirect('profile')
    else:
        user_settings = {'user_settings': Participants.objects.all().get(user=request.user)}
        form = UpdateUserForm(**user_settings)
    return render(request, 'edit_profile.html',
                  context={'form': form})
