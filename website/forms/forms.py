from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import Field, Form, CharField, TextInput, Textarea, ModelForm

from ..models import Participants


class RequirementField(Field):
    def __init__(self, required, label, initial, widget, help_text):
        super().__init__(required=required, label=label, initial=initial, widget=widget, help_text=help_text)

    def clean(self, value):
        return value


from django.forms import widgets
from ..requirements.requirement import *


class RequirementWidget(widgets.MultiWidget):
    def __init__(self, attrs=None):
        tmp = []
        for val in GLOBAL_REQUIREMENTS.values():
            if isinstance(val, Choice):
                t = [(c, c) for c in val.choices()]
                widget = widgets.Select(choices=t, attrs={'widget_name': val.name})
                widget.template_name = 'custom_choice_widget.html'
                tmp.append(widget)

        _widgets = tuple(tmp)
        super().__init__(_widgets, attrs)

    def decompress(self, value):
        if value:
            return [value['eye_color']]
        return [None]

    def value_from_datadict(self, data, files, name):
        collected_values = [
            widget.value_from_datadict(data, files, name + '_%s' % i) for i, widget in enumerate(self.widgets)]
        try:
            ret = Requirements({'eye_color': Value('eye_color', collected_values[0])})
        except ValueError:
            return ''
        else:
            return ret


class RegisterForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    requirements = RequirementField(required=True, label='Requirements', initial=GLOBAL_REQUIREMENTS,
                                    widget=RequirementWidget,
                                    help_text="State your requirements here!")

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'requirements')


class UpdateUserForm(ModelForm):
    requirements = RequirementField(required=True, label='Requirements', initial=GLOBAL_REQUIREMENTS,
                                    widget=RequirementWidget,
                                    help_text="State your requirements here!")

    class Meta:
        model = User
        fields = ('email',)

    def __init__(self, *args, **kwargs):
        user_settings: Participants = kwargs.pop('user_settings', {})
        if user_settings:
            initial = {
                'email': user_settings.user.email
            }
            kwargs['initial'] = initial
        super().__init__(*args, **kwargs)


class CreateStudyForm(Form):
    title = CharField(label="Title", help_text="What's the subject of you study?",
                      widget=TextInput(attrs={"placeholder": "Enter title here",
                                              "class": "form-control",
                                              "aria-describedby": "title_help"}))
    reward = CharField(label="Reward", help_text="What does the participant get for participating?",
                       widget=TextInput(attrs={"placeholder": "Enter reward here",
                                               "class": "form-control",
                                               "aria-describedby": "reward_help"}))
    description = CharField(label="Description",
                            help_text="Briefly describe your study or give additional information.",
                            widget=Textarea(attrs={"placeholder": "Enter description here",
                                                   "class": "form-control",
                                                   "aria-describedby": "description_help"}))
    requirements = RequirementField(required=True, label='Requirements', initial=GLOBAL_REQUIREMENTS,
                                    widget=RequirementWidget,
                                    help_text="State the necessary requirements here!")
