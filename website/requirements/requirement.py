import collections
import abc

import toml
from django.conf import settings


class Requirement(abc.ABC):
    """
    :Requirement: base class for a single requirement
    :name: name of the requirement, e.g. age
    """
    name: str = ''

    def __init__(self, name):
        self.name = name

    @abc.abstractmethod
    def get_name(self):
        pass


class Range(Requirement):
    """
    :Range: represents a range requirement, bounded by @lower and @upper.
    :lower: lower bound of the range.
    :upper: upper bound of the range.

    """

    lower = 0
    upper = 0

    def __init__(self, name, lower, upper):
        super().__init__(name)
        self.lower = lower
        self.upper = upper

    def __str__(self):
        return f'{self.name}: range from {self.lower} to {self.upper}'

    def __eq__(self, other):
        if isinstance(other, Range):
            return self.lower == other.lower and self.upper == other.upper
        elif isinstance(other, Choice):
            # what about this?
            # for c in other.choices():
            #     if not self.lower <= c <= self.upper:
            #         return False
            # return True
            raise NotImplementedError
        elif isinstance(other, Value):
            return self.lower <= other.value <= self.upper
        elif isinstance(other, Boolean):
            # makes no sense, right?
            raise NotImplementedError

    def get_name(self):
        return 'Range'

    def get_lower(self):
        return self.lower

    def get_upper(self):
        return self.upper


class Choice(Requirement):
    """
    :Choice: represents a set of different choices.
    """
    _choices: list = []

    def __init__(self, name, choices):
        super().__init__(name)
        self._choices = choices

    def __str__(self):
        return f'{self.name}: choice of {self._choices}'

    def __eq__(self, other):
        if isinstance(other, Range):
            raise NotImplementedError
        elif isinstance(other, Choice):
            return set(self.choices()) == set(other.choices())
        elif isinstance(other, Value):
            return other.value in self.choices()
        elif isinstance(other, Boolean):
            raise NotImplementedError

    def __iter__(self):
        return self._choices.__iter__()

    def choices(self):
        """
        Returns all contained values.
        :rtype: list
        :return: values of the @Choice
        """
        return self._choices

    def get_name(self):
        return 'Choice'


class Boolean(Requirement):
    """
    :Boolean: represents a @Requirement which can be either true or false.
    """
    value = False

    def __init__(self, name, value):
        super().__init__(name)
        self.value = value

    def __str__(self):
        return f'{self.name}: boolean: {self.value}'

    def __eq__(self, other):
        if isinstance(other, Range):
            raise NotImplementedError
        elif isinstance(other, Choice):
            # what about this?
            # return self.value in other.choices()
            raise NotImplementedError
        elif isinstance(other, Value):
            raise NotImplementedError
        elif isinstance(other, Boolean):
            return self.value == other.value

    def get_name(self):
        return 'Boolean'


class Value(Requirement):
    value = None

    def __init__(self, name, value):
        super().__init__(name)
        self.value = value

    def __str__(self):
        return f'{self.name}: value: {self.value}'

    def __eq__(self, other):
        if isinstance(other, Range):
            return other.lower <= self.value <= other.upper
        elif isinstance(other, Choice):
            return self.value in other.choices()
        elif isinstance(other, Value):
            return self.value == other.value
        elif isinstance(other, Boolean):
            return NotImplementedError

    def get_name(self):
        return 'Value'


class Requirements:
    """
    :Requirements: data structure representing the different requirements of a study or a user, mapping the name of a
     requirement to a concrete instance of @Requirement. For example:

     Requirements(
        {
            'age': Range('age', 18, 99),
            'eye_color': Choice('eye_color', ['blue', 'green'])
        }
    )
    """
    _inner: dict = {}

    def __init__(self, req):
        self._inner = req

    def __str__(self):
        return str(self._inner)

    def __getitem__(self, item):
        return self._inner.__getitem__(item)

    def __iter__(self):
        return self._inner.__iter__()

    def keys(self):
        return self._inner.keys()

    def values(self):
        return self._inner.values()

    def items(self):
        return self._inner.items()


def toml_to_requirements(p):
    """
    Load the global requirements config and creates a @Requirements object from it.
    :param p: path to the config
    :return: the @Requirements object created from @p
    """
    with open(p, 'r') as f:
        toml_string = f.read()
        parsed_toml = toml.loads(toml_string)
    toml_requirements: dict = parsed_toml['global']
    requirements = collections.OrderedDict()
    for key, val in toml_requirements.items():
        if isinstance(val, list):
            if True in val and False in val:
                tmp = Boolean(key, val)
            else:
                tmp = Choice(key, val)
        elif isinstance(val, dict):
            tmp = Range(key, val['min'], val['max'])
        else:
            raise Exception
        requirements[key] = tmp
    return Requirements(requirements)


GLOBAL_REQUIREMENTS = toml_to_requirements(settings.REQUIREMENTS_CONFIG)
