from django import template

from website.models import Participants
from website.requirements.requirement import Requirement

register = template.Library()


@register.filter
def is_requirement(value, arg):
    return value.__class__ == f'website.requirements.requirements.{arg}'


@register.filter
def get_type(value):
    if not isinstance(value, Requirement):
        'not implemented'
    else:
        return value.get_name()


@register.filter
def contains(value, user):
    return len(value.filter(user=user)) > 0

