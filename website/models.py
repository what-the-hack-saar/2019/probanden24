import pickle

from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from .requirements.requirement import Requirements


class RequirementsField(models.TextField):

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return pickle.loads(value)

    def to_python(self, value):
        if isinstance(value, Requirements):
            return value

        if value is None:
            return value

        return pickle.loads(value)

    def get_db_prep_value(self, value, connection, prepared=False):
        return pickle.dumps(value)


class Participants(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, unique=True)
    requirements = RequirementsField()

    def __str__(self):
        return self.user.username

    @receiver(post_save, sender=User)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Participants.objects.create(user=instance)
        instance.participants.save()


class Study(models.Model):
    title = models.CharField(max_length=40, null=False, blank=False)

    creator = models.ForeignKey(Participants, on_delete=models.CASCADE, blank=False,
                                null=False, related_name='created_studies', unique=False)
    description = models.TextField(blank=True, default="")
    reward = models.CharField(max_length=10, null=True)
    requirements = RequirementsField()
    participants = models.ManyToManyField(Participants, related_name='registered_studies',
                                          blank=True)
