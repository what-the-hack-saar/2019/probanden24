# probanden24
This project has the purpose to simplify recruiting of participants for studies and to moreover make it easier for participants to find studies and to manage their participations.  

It is intended to be used e.g. by psycology departments of universities.

## Features
- Show available studies on index page
- Show details of a specific study and book your participation
- Registration and Login
- Show personal page with registered and created studies
- Add studies with name, reward, description
- Requirement Model that will allow to compare user attributes with study requirements to find matching studies

## Coming
- Email Notifications for users if a matching study is added
- History of participated and hosted studies in personal page
- Beauty and stuff

## Dev Environment

It is reasonable to expect this project to be hosted on an Ubuntu 18.04 for the future,
thus we need to keep Python3.6 compatibility. 

Assuming a clean system, this service requires the dependencies in [the requirements file.](requirements.txt)

The project is supposed to be run in a python3 virtual environment:

```bash
sudo apt install python3-pip virtualenv

virtualenv -p python3 venv
. venv/bin/activate
pip install -r requirements.txt
```

This project uses Postgres as database backend, so install and configure it accordingly.
This documentation assumes a Postgres database listening on localhost containing
a database called 'probanden24' owned by Postgres-user 'django' identified by
'djangopassword'. If your setup differs, adjust [the settings file](probanden24/settings.py) accordingly.

Additionally, ensure that [the settings file](probanden24/settings.py) is set up correctly to allow the host you are running the service on.

Note: for development you can set the environment variable SQLITE to any value to use a local SQLite database. 
